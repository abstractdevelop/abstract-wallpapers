# Mountain and Birds
*Mountain and Birds* is a abstract style wallpaper showing several mountains, a setting sun and three birds flying on top of the mountains and sun.

![Mountain and Birds](Mountains and Birds Preview.png)
