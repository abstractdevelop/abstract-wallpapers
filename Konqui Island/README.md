# Island Konqui
*Island Konqui* is a wallpaper showing a chain of islands in the "Ice Cold" style representing Konqui.  
There are two variants of *Island Konqui*, one with a tail of smaller islands and one without. The idea is that you use the Konqui without a tail on your lockscreen and login screen, and the Konqui with tail on your desktop.  

## Konqui without tail
![Konqui Island Desktop without Tail](Konqui Island Preview.2.png)  

## Konqui with tail
![Konqui Island Desktop with Tail](Konqui Island Preview.png)  
